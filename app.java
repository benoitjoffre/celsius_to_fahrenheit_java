import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Choisissez votre conversion 1 (Fahrenheit => Celsius) | 2 (Celsius => Fahrenheit) : ");
        int conversion = Integer.parseInt(reader.readLine());

        switch(conversion) {
            case 1:
                System.out.print("Veuillez rentrer une temperature en Fahrenheit: ");
                double fahrenheitToCelsius = convert(Double.parseDouble(reader.readLine()), false);
                System.out.println("La temperature en Celsius est de : " + fahrenheitToCelsius);
            break;

            case 2:
                System.out.print("Veuillez rentrer une temperature en Celsius: ");
                double celsiusToFahrenheit = convert(Double.parseDouble(reader.readLine()), true);
                System.out.println("La temperature en Fahrenheit est de : "+ celsiusToFahrenheit);
            break;

            default:
                System.out.println("Vous devez choisir une conversion existante !");
            break;
        }
    }
    
    public static double convert(double temperature, boolean toFahrenheit) {
        
        double temp = (9.0/5.0) * temperature;
        if (toFahrenheit) return temp + 32;
        return temp - 32;
    }
    
}